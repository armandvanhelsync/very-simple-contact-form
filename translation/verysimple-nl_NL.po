msgid ""
msgstr ""
"Project-Id-Version: Very Simple Contact Form\n"
"POT-Creation-Date: 2015-07-27 13:17+0200\n"
"PO-Revision-Date: 2015-07-31 16:24+0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.3\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: vscf_settings.php\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;"
"_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: vscf_main.php:11 vscf_widget_form.php:11
msgid "Name"
msgstr "Naam"

#: vscf_main.php:12 vscf_widget_form.php:12
msgid "Email"
msgstr "E-mailadres"

#: vscf_main.php:13 vscf_widget_form.php:13
msgid "Subject"
msgstr "Onderwerp"

#: vscf_main.php:14 vscf_widget_form.php:14
msgid "Message"
msgstr "Bericht"

#: vscf_main.php:15 vscf_widget_form.php:15
msgid "Fill in number"
msgstr "Vul in nummer"

#: vscf_main.php:16 vscf_widget_form.php:16
msgid "Submit"
msgstr "Verstuur"

#: vscf_main.php:17 vscf_widget_form.php:17
msgid "Please fill in all the required fields"
msgstr "Svp alle verplichte velden invullen"

#: vscf_main.php:18 vscf_main.php:19 vscf_widget_form.php:18
#: vscf_widget_form.php:19
msgid "Please enter at least 2 characters"
msgstr "Svp tenminste 2 tekens invullen"

#: vscf_main.php:20 vscf_widget_form.php:20
msgid "Please enter at least 10 characters"
msgstr "Svp tenminste 10 tekens invullen"

#: vscf_main.php:21 vscf_widget_form.php:21
msgid "Please fill in the correct number"
msgstr "Svp juiste nummer invullen"

#: vscf_main.php:22 vscf_widget_form.php:22
msgid "Please enter a valid email"
msgstr "Svp geldig e-mailadres invullen"

#: vscf_main.php:23 vscf_widget_form.php:23
msgid "Thanks for your message! I will contact you as soon as I can."
msgstr "Bedankt voor je bericht. Ik neem zo snel mogelijk contact met je op."

#: vscf_settings.php:86
msgid "VSCF Custom Style"
msgstr "VSCF Aangepaste Stijl"

#: vscf_settings.php:94
msgid "Description"
msgstr "Beschrijving"

#: vscf_settings.php:95
msgid "Custom Style"
msgstr "Aangepaste Stijl"

#: vscf_settings.php:101
msgid ""
"On this page you can add Custom Style (CSS) to change the layout of your "
"Very Simple Contact Form."
msgstr ""
"Op deze pagina kun je Aangepaste Stijl (CSS) invoeren om het uiterlijk van "
"je Very Simple Contact Form te veranderen."

#. translators: plugin header field 'Name'
#: vscf_settings.php:116 vscf_widget.php:8 vscf_widget.php:41
msgid "Very Simple Contact Form"
msgstr ""

#: vscf_settings.php:120
msgid "Save Style"
msgstr "Stijl Opslaan"

#: vscf_settings.php:126
msgid "Field label"
msgstr "Veld label"

#: vscf_settings.php:128
msgid "All fields"
msgstr "Alle velden"

#: vscf_settings.php:130
msgid "Fields by name"
msgstr "Velden op naam"

#: vscf_settings.php:133
msgid "Submit button"
msgstr "Verzendknop"

#: vscf_settings.php:138
msgid "Field error"
msgstr "Veld fout"

#: vscf_settings.php:140
msgid "Field label error"
msgstr "Veld label fout"

#: vscf_settings.php:142
msgid "Error and Thank You message"
msgstr "Foutmelding en Dank Je bericht"

#: vscf_settings.php:144
msgid "Widget"
msgstr "Widget"

#: vscf_settings.php:146
msgid "Plugin Stylesheet"
msgstr "Plugin Stylesheet"

#: vscf_settings.php:147
msgid "For Default Style"
msgstr "Voor Standaard Stijl"

#: vscf_settings.php:147
msgid "click here"
msgstr "klik hier"

#: vscf_widget.php:7
msgid "Very Simple Contact Form sidebar widget"
msgstr ""

#: vscf_widget.php:45
msgid "Widget Title:"
msgstr "Widget Titel:"

#. translators: plugin header field 'Description'
msgid ""
"This is a very simple contact form. Use shortcode [contact] to display form "
"on page or use the widget. For more info please check readme file."
msgstr ""
"Dit is een zeer eenvoudig contactformulier. Gebruik shortcode [contact] om "
"formulier op pagina te plaatsen of gebruik de widget. Voor meer info zie "
"readme bestand."

#. translators: plugin header field 'Author'
msgid "Guido van der Leest"
msgstr ""

#. translators: plugin header field 'AuthorURI'
msgid "http://www.guidovanderleest.nl"
msgstr ""
